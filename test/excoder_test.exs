Code.require_file "test_helper.exs", __DIR__

defmodule ExcoderTest do
    use ExUnit.Case

    test "Decode single entity" do
        assert(ExCoder.decode("&amp;") == "&")
    end

    test "Decode numeric entity" do
        assert(ExCoder.decode("&#4352;") == "ᄀ")
    end

    test "Decode hexadecimal entity" do
        assert(ExCoder.decode("&#x123;") == "ģ")
    end

    test "Decode invalid hexadecimal" do
        assert(ExCoder.decode("&#x1h23;") == "&#x1h23;")
    end

    test "Decode invalid numeric" do
        assert(ExCoder.decode("&#453g3;") == "&#453g3;")
    end

    test "Decode numeric that is too big" do
        assert(ExCoder.decode("&#1114112;") == "&#1114112;")
    end

    test "Decode HTML" do
        assert(ExCoder.decode("""
<div class='line' id='LC9'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="ss">address:</span> <span class="s2">&quot;chat.eu.freenode.net&quot;</span><span class="p">,</span></div><div class='line' id='LC10'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="ss">
"""
) == """
<div class='line' id='LC9'>      <span class="ss">address:</span> <span class="s2">"chat.eu.freenode.net"</span><span class="p">,</span></div><div class='line' id='LC10'>      <span class="ss">
"""
        )
    end

    test "Decode invalid case" do
        assert(ExCoder.decode("&AOPF;") == "&AOPF;")
    end





    test "Encode simple text" do
        assert(ExCoder.encode("foo and bar") == "foo and bar")
    end

    test "Don't encode linebreaks" do
        assert(ExCoder.encode("\nfoo\nand\nbar\n") == "\nfoo\nand\nbar\n")
    end

    test "Encode scandinavian characters" do
        assert(ExCoder.encode("Hääyöaie liittyy öylättiin, sanoi Åke.") == "H&auml;&auml;y&ouml;aie liittyy &ouml;yl&auml;ttiin&comma; sanoi &Aring;ke&period;")
    end

    test "Encode apple signs" do
        assert(ExCoder.encode("⌘⌥⇧ and the last one is ⎋") == "&#xF8FF;&#x2318;&#x2325;&#x21E7; and the last one is &#x238B;")
    end

    test "Encode decode roundtrip with very difficult unicode (HE COMES)" do
        zalgo = "Z͉̘͕͐ḁ̗̕ͅl̟̥̳̞̞͔ͬ̔̂̋̾gͮͫ̓̓̕o͎͉̹͕͌͂̍͐̌̋ ͕̖͖ͯĩ͇̹̬̤͕̟ͭ̇ș̼̠̹̒̂ͭͯ̓ ͉̪͍͚̗̟͚ḟ̴̙̟̯͚̭̳̼̈́û̔ͥ̒nͥͩͭ̎̃!̫͔ ̲̯̰̰̗͔ͪ͝R̭͉̬͓̜͉ͮͬͤ̃ͯ͊a̡̩̍͊i̖̺̝̻ͦ̋̾͑͌̇ņ͍̖̟͚͓ͧ͒b̻̹͉͉̘̎̄ͬ̆͗͠ơ̮̱͉͓̗̝̯ẅ͍̱́̋ͧͭ̾̐̇s̯͓̦͓̦͇͚̎ͦ̌͐̾ ̟̞̑̉̈̎̈́l̯͉̯ͭ͡ȏ̟͙̯̞̫̳̮͒ͭ̎͆l̼͒ͩ̌̀͒l̒͒ͭ̌ͩ̎͘ͅi̝̲ͬ̋̾̉̋p̢̣͉͒̾͗ͨͯö͙̳̘̜̓ͣ͒́̿ͫͅp̵͔͎͓sͅ.̶̹͙̼̺ͧ͂̒"
        assert(ExCoder.decode(ExCoder.encode(zalgo)) == zalgo)
    end
end
