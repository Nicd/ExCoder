ExCoder is a small Elixir library for encoding and decoding HTML entities. I
decided to write it because I needed to decode HTML entities (without caring
about the mess of parsing HTML) and I did not want to use a full XML library
from the Erlang side.

ExCoder supports both decoding named and numbered HTML entities. Encoding will
use named entities where possible and fall back to hexadecimal encoded entities.
